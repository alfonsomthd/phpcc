<?php

namespace Alfonsomthd\Phpcc;

use Symfony\Component\Console\Application;
use Symfony\Component\Console\Input\InputInterface;

class PhpccApplication extends Application
{
    protected function getCommandName(InputInterface $input)
    {
        return PhpccCommand::COMMAND_NAME;
    }

    protected function getDefaultCommands()
    {
        $defaultCommands = parent::getDefaultCommands();

        $defaultCommands[] = $this->getApplicationCommand();

        return $defaultCommands;
    }

    protected function getApplicationCommand()
    {
        return new PhpccCommand();
    }
}
