<?php

namespace Alfonsomthd\Phpcc\Check;

use Alfonsomthd\Phpcc\Check\Exception\CodeStyleException;

class CodeStyleCheck extends Check
{
    const PHPCSFIXER_DEFAULT_RULES = '@PSR2,@PSR1,-psr0';

    public function check()
    {
        $this->output->writeln('<comment>Checking code style with CS Fixer</comment>');

        $fullDisplay = '';
        $command = [
            PHP_BINARY,
            $this->config['vendor-dir'].'/bin/php-cs-fixer',
            'fix',
            '-v',
            '--using-cache=no',
        ];

        $csFixerRules = isset($this->config['checks']['php-cs-fixer']['rules']) ?
            trim((string) $this->config['checks']['php-cs-fixer']['rules']) : self::PHPCSFIXER_DEFAULT_RULES;
        if ('' !== $csFixerRules) {
            $command[] = '--rules=' . $csFixerRules;
        }

        $csFixerFix = isset($this->config['checks']['php-cs-fixer']['fix']) ?
            (bool) $this->config['checks']['php-cs-fixer']['fix'] : false;
        if (!$csFixerFix) {
            $command[] = '--dry-run';
        }

        foreach ($this->files as $file) {
            try {
                $this->runProcess(array_merge($command, [$file]), false, false);
            } catch (\Throwable $error) {
                $fullDisplay .= $error->getMessage();
            }
        }

        if (isset($error)) {
            if ($this->isCheckException($error)) {
                throw new CodeStyleException($fullDisplay);
            }

            throw $error;
        }
    }
}
