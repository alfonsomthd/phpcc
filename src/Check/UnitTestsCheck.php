<?php

namespace Alfonsomthd\Phpcc\Check;

class UnitTestsCheck extends Check
{
    const PHPUNIT_DEFAULT_ALLOW_DISPLAY = true;

    public function check()
    {
        $this->output->writeln('<comment>Running tests with PHPUnit...</comment>');

        $phpunitBinary = 'vendor/bin/phpunit';

        if (!file_exists($phpunitBinary)) {
            $this->output->writeln('<comment>'.$phpunitBinary.' not found!!!</comment>');
            return;
        }

        $phpunitConfigFile = isset($this->config['checks']['phpunit']['config-file']) ?
            trim((string) $this->config['checks']['phpunit']['config-file']) : '';

        if (('' === $phpunitConfigFile || !file_exists($phpunitConfigFile))
            && !file_exists('phpunit.xml') && !file_exists('phpunit.xml.dist')) {
            $this->output->writeln('<comment>PHPUnit configuration file not found!!!</comment>');
            return;
        }

        $command = [
            PHP_BINARY,
            $phpunitBinary,
        ];

        if ('' !== $phpunitConfigFile) {
            $command[] = '-c';
            $command[] = $phpunitConfigFile;
        }

        $realTimeDisplay = self::PHPUNIT_DEFAULT_ALLOW_DISPLAY;
        if (isset($this->config['checks']['phpunit']['allow-display'])) {
            $realTimeDisplay = trim((string) $this->config['checks']['phpunit']['allow-display']);
        }

        $realTimeStandardOutput = false;
        if (!empty($realTimeDisplay) && 'false' !== $realTimeDisplay) {
            $realTimeStandardOutput = true;
        }

        $this->runProcess($command, false, $realTimeStandardOutput);
    }
}
