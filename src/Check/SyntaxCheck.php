<?php

namespace Alfonsomthd\Phpcc\Check;

use Alfonsomthd\Phpcc\Check\Exception\SyntaxException;

class SyntaxCheck extends Check
{
    public function check()
    {
        $this->output->writeln('<comment>Checking PHP syntax</comment>');

        $fullDisplay = '';
        foreach ($this->files as $file) {
            $command = [
                PHP_BINARY,
                '-l',
                $file,
            ];

            try {
                $this->runProcess($command);
            } catch (\Throwable $error) {
                $fullDisplay .= $error->getMessage();
            }
        }

        if (isset($error)) {
            if ($this->isCheckException($error)) {
                throw new SyntaxException($fullDisplay);
            }

            throw $error;
        }
    }
}
