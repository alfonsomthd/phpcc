<?php

namespace Alfonsomthd\Phpcc\Check\Exception;

class CodeMessException extends CheckNotPassedException
{
    const CODE = 5;
}
