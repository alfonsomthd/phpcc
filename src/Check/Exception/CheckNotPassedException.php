<?php

namespace Alfonsomthd\Phpcc\Check\Exception;

class CheckNotPassedException extends \Exception
{
    const CODE = 2;

    public function __construct($message = "")
    {
        parent::__construct($message, static::CODE, null);
    }
}
