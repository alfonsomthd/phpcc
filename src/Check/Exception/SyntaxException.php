<?php

namespace Alfonsomthd\Phpcc\Check\Exception;

class SyntaxException extends CheckNotPassedException
{
    const CODE = 3;
}
