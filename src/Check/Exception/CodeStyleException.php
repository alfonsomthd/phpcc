<?php

namespace Alfonsomthd\Phpcc\Check\Exception;

class CodeStyleException extends CheckNotPassedException
{
    const CODE = 4;
}
