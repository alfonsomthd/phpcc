<?php

namespace Alfonsomthd\Phpcc\Check;

use Alfonsomthd\Phpcc\Check\Exception\CheckNotPassedException;
use Alfonsomthd\Phpcc\PhpccProcess;
use Alfonsomthd\Phpcc\PhpccProcessException;
use Symfony\Component\Console\Output\OutputInterface;

abstract class Check
{
    const SYNTAX_CHECK = 'SYNTAX_CHECK';
    const CODE_STYLE_CHECK = 'CODE_STYLE_CHECK';
    const PHPMD_CHECK = 'PHPMD_CHECK';
    const UNIT_TESTS_CHECK = 'UNIT_TESTS_CHECK';

    protected $output;
    protected $process;
    protected $files;
    protected $config;

    abstract public function check();

    private function __construct(OutputInterface $output, array $files, array $config)
    {
        $this->output = $output;
        $this->process = PhpccProcess::create($this->output);
        $this->files = $files;
        $this->config = $config;
    }

    public static function create($checkType, OutputInterface $output, array $files, array $config)
    {
        switch ($checkType) {
            case self::SYNTAX_CHECK:
                return new SyntaxCheck($output, $files, $config);
                break;
            case self::CODE_STYLE_CHECK:
                return new CodeStyleCheck($output, $files, $config);
                break;
            case self::PHPMD_CHECK:
                return new PhpMdCheck($output, $files, $config);
                break;
            case self::UNIT_TESTS_CHECK:
                return new UnitTestsCheck($output, $files, $config);
                break;
            default:
                throw new \InvalidArgumentException('Invalid check: "'.$checkType.'"');
        }
    }

    protected function runProcess(
        array $command,
        $realTimeErrorOutput = true,
        $realTimeStandardOutput = false
    ) {
        try {
            $this->process->execute($command, $realTimeErrorOutput, $realTimeStandardOutput);
        } catch (PhpccProcessException $error) {
            throw new CheckNotPassedException($error->getMessage());
        }
    }

    protected function isCheckException(\Throwable $error)
    {
        return CheckNotPassedException::class === get_class($error);
    }
}
