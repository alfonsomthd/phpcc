<?php

namespace Alfonsomthd\Phpcc\Check;

use Alfonsomthd\Phpcc\Check\Exception\CodeMessException;

class PhpMdCheck extends Check
{
    const PHPMD_DEFAULT_PRIORITY = 2;

    public function check()
    {
        $this->output->writeln('<comment>Checking code mess with PHPMD</comment>');

        $phpmdDefaultRuleSet = $this->config['install-dir'].'/phpmdRuleSet.xml';

        $phpmdRuleSet = isset($this->config['checks']['phpmd']['ruleset']) ?
            trim((string) $this->config['checks']['phpmd']['ruleset']) : $phpmdDefaultRuleSet;
        if ('' === $phpmdRuleSet) {
            $phpmdRuleSet = $phpmdDefaultRuleSet;
        }
        $phpmdPriority = isset($this->config['checks']['phpmd']['minimumpriority']) ?
            (int) $this->config['checks']['phpmd']['minimumpriority'] : self::PHPMD_DEFAULT_PRIORITY;
        if (empty($phpmdPriority)) {
            $phpmdPriority = self::PHPMD_DEFAULT_PRIORITY;
        }

        $command = [
            PHP_BINARY,
            $this->config['vendor-dir'].'/bin/phpmd',
            implode(',', $this->files),
            'text',
            $phpmdRuleSet,
            '--minimumpriority',
            $phpmdPriority,
        ];

        try {
            $this->runProcess($command, false, true);
        } catch (\Throwable $error) {
            if ($this->isCheckException($error)) {
                throw new CodeMessException($error->getMessage());
            }

            throw $error;
        }
    }
}
