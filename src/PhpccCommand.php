<?php

namespace Alfonsomthd\Phpcc;

use Alfonsomthd\Phpcc\Check\Check;
use Alfonsomthd\Phpcc\Check\Exception\CheckNotPassedException;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class PhpccCommand extends Command
{
    const COMMAND_NAME = 'phpcc';
    const COMMAND_CONFIG_FILE = self::COMMAND_NAME.'.json';
    const STATUS_CODE_SUCCESS = 0;
    const STATUS_CODE_ERROR = 1;
    const PHP_FILE_EXTENSION_PATTERN = '/\.php$/';
    const MESSAGE_SEARCHING = 'Searching for PHP files in ';
    const MESSAGE_FILE_DOES_NOT_EXIST = 'File does not exist';
    const MESSAGE_NO_FILES_TO_CHECK = 'No PHP files to check';

    private $config;
    /** @var OutputInterface */
    private $output;
    protected $checksToRun = [
        Check::SYNTAX_CHECK,
        Check::CODE_STYLE_CHECK,
        Check::UNIT_TESTS_CHECK,
        Check::PHPMD_CHECK,
    ];

    public function __construct()
    {
        $installDir = substr(__DIR__, 0, strripos(__DIR__, '/src'));

        $this->config = [
            'install-dir' => $installDir,
            'vendor-dir' => file_exists($installDir.'/vendor') ? $installDir.'/vendor' : realpath($installDir.'/../..'),
        ];

        parent::__construct(self::COMMAND_NAME);
    }

    protected function configure()
    {
        $this->setName(self::COMMAND_NAME)
            ->setDescription('A PHP code checks automation tool that can be used as git pre-commit hook.')
            ->setHelp(file_get_contents($this->config['install-dir'].'/README.md'))
        ;

        $this->addArgument(
            'TARGET',
            null,
            'File or directory. If empty, it will check PHP files in git staging area.'
        );

        $this->addOption(
            'fix-cs',
            null,
            null,
            'Fix found code style errors.'
        );
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $this->output = $output;

        try {
            if (extension_loaded('xdebug')) {
                $this->output->writeln('<bg=yellow>Warning: Xdebug is enabled. This can cause runtime problems.</>');
            }

            $this->getUserDefinedConfig();
            if ($input->getOption('fix-cs')) {
                $this->config['checks']['php-cs-fixer']['fix'] = true;
            }
            $target = trim($input->getFirstArgument());
            $phpFiles = $this->getPhpFiles($this->getFiles($target));

            if (empty($phpFiles)) {
                $this->output->writeln('<comment>'.self::MESSAGE_NO_FILES_TO_CHECK.'.</comment>');

                return self::STATUS_CODE_SUCCESS;
            }

            if (!$this->isStagingCheck($target)) {
                $onlyStagingCheck = array_search(Check::UNIT_TESTS_CHECK, $this->checksToRun);
                unset($this->checksToRun[$onlyStagingCheck]);
            } elseif (!isset($this->config['checks']['php-cs-fixer']['fix'])) {
                $this->config['checks']['php-cs-fixer']['fix'] = true;
            }

            foreach ($this->checksToRun as $checkType) {
                $check = Check::create($checkType, $this->output, $phpFiles, $this->config);
                $check->check();
                if (Check::CODE_STYLE_CHECK === $checkType && $this->isStagingCheck($target)) {
                    PhpccProcess::create($this->output)->execute(array_merge(['git', 'add'], $phpFiles));
                }
            }

            $this->output->writeln('<info>PHP checks passed! Congratulations!</info>');

            return self::STATUS_CODE_SUCCESS;
        } catch (\Throwable $error) {
            return $this->handleError($error);
        }
    }

    private function getUserDefinedConfig()
    {
        $userDefinedConfig = file_exists(self::COMMAND_CONFIG_FILE) ?
            json_decode(file_get_contents(self::COMMAND_CONFIG_FILE), true) : [];

        if (is_null($userDefinedConfig)) {
            throw new \Exception(self::COMMAND_CONFIG_FILE.' does not contain a valid JSON string.');
        }

        $this->config += $userDefinedConfig;
    }

    private function getFiles($target)
    {
        if ($this->isStagingCheck($target)) {
            $this->output->writeln('<comment>'.self::MESSAGE_SEARCHING.'git staging area</comment>');

            $process = PhpccProcess::create($this->output);

            $files = $process->execute([
                'git',
                'diff',
                '--cached',
                '--name-only',
                '--diff-filter=ACMRTUXB',
            ]);
        } elseif (is_dir($target)) {
            $this->output->writeln('<comment>'.self::MESSAGE_SEARCHING.$target.'</comment>');

            $process = PhpccProcess::create($this->output);

            $files = $process->execute([
                'find',
                $target,
                '-type',
                'f',
                '-name',
                '*.php',
            ]);
        } else {
            $files = (array) $target;
        }

        return $files;
    }

    private function isStagingCheck($target)
    {
        return empty($target);
    }

    private function getPhpFiles($files)
    {
        $phpFiles = [];

        foreach ($files as $file) {
            if (!$this->hasPhpFileExtension($file)) {
                continue;
            }

            if (!file_exists($file)) {
                $this->output->writeln(self::MESSAGE_FILE_DOES_NOT_EXIST.': '.$file);
                continue;
            }

            if (!$this->fileHasToBeChecked($file)) {
                continue;
            }

            $phpFiles[] = $file;
        }

        return $phpFiles;
    }

    private function hasPhpFileExtension($file)
    {
        return preg_match(self::PHP_FILE_EXTENSION_PATTERN, $file);
    }

    private function fileHasToBeChecked($file)
    {
        $hasToBeChecked = true;

        if (!empty($this->config['ignore'])) {
            foreach ((array) $this->config['ignore'] as $ignorePath) {
                $ignorePath = (string) $ignorePath;

                $negationPrefix = false;
                if ('!' === substr($ignorePath, 0, 1)) {
                    $negationPrefix = true;
                    $ignorePath = substr($ignorePath, 1);
                }

                if (!file_exists($ignorePath)) {
                    $this->output->writeln('<bg=yellow>Warning: ignore path "'.$ignorePath.'" does not exist.</>');
                    continue;
                }

                if (false !== stripos(realpath($file), realpath($ignorePath))) {
                    $hasToBeChecked = $negationPrefix;
                }
            }
        }

        return $hasToBeChecked;
    }

    private function handleError(\Throwable $error)
    {
        $this->output->writeln($error->getMessage());

        $errorMessage = 'Checks stopped';
        $errorCode = self::STATUS_CODE_ERROR;
        if ($error instanceof CheckNotPassedException) {
            $errorMessage = 'Checks not passed';
            $errorCode = $error->getCode();
        }

        $this->output->writeln('<error>' . $errorMessage . '!</error>');

        return $errorCode;
    }
}
