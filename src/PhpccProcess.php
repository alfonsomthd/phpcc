<?php

namespace Alfonsomthd\Phpcc;

use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Process\Process;

class PhpccProcess
{
    const PROCESS_DEFAULT_TIMEOUT = null;

    /** @var OutputInterface */
    private $output;

    private function __construct(OutputInterface $processOutput)
    {
        $this->output = $processOutput;
    }

    public static function create(OutputInterface $output)
    {
        return new self($output);
    }

    public function execute(
        array $command,
        $realTimeErrorOutput = true,
        $realTimeStandardOutput = false
    ) {
        $process = new Process($command);
        //$this->output->writeln($process->getCommandLine());
        $process->setTimeout(self::PROCESS_DEFAULT_TIMEOUT);

        $realTimeErrorOutput = (bool) $realTimeErrorOutput;
        $realTimeStandardOutput = (bool) $realTimeStandardOutput;
        $realTimeDisplayCallback = null;
        if ($realTimeErrorOutput || $realTimeStandardOutput) {
            $realTimeDisplayCallback =
                function ($outputType, $output) use ($realTimeErrorOutput, $realTimeStandardOutput) {
                    if (($realTimeErrorOutput && Process::ERR === $outputType)
                        || ($realTimeStandardOutput && Process::OUT === $outputType)) {
                        $this->output->write($output);
                    }
                };
        }

        $process->run($realTimeDisplayCallback);

        $processDisplay = $process->getOutput();

        if (!$process->isSuccessful()) {
            $processErrorDisplay = $process->getErrorOutput();

            if (empty(trim($processDisplay))) {
                $processDisplay = $processErrorDisplay;
            }

            if (!is_null($realTimeDisplayCallback)) {
                $processDisplay = '';
            }

            throw new PhpccProcessException($processDisplay);
        }

        return explode(PHP_EOL, trim($processDisplay));
    }
}
