<?php

namespace Alfonsomthd\Phpcc\Tests;

use Alfonsomthd\Phpcc\PhpccApplication;
use Alfonsomthd\Phpcc\PhpccCommand;
use Alfonsomthd\Phpcc\Check\Exception\SyntaxException;
use Alfonsomthd\Phpcc\Check\Exception\CodeStyleException;
use Alfonsomthd\Phpcc\Check\Exception\CodeMessException;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Console\Tester\ApplicationTester;

class PhpccApplicationTest extends TestCase
{
    /** @var ApplicationTester */
    private $application;
    /** @var string */
    private $checkFile;

    protected function setUp()
    {
        $application = new PhpccApplication();
        $application->setAutoExit(false);

        $this->application = new ApplicationTester($application);
    }

    /**
     * @test
     */
    public function fileWithoutPhpExtensionShouldDisplayNoPhpFilesMessage()
    {
        $this->application->run([PhpccCommand::COMMAND_NAME]);

        $this->assertNotContains(PhpccCommand::MESSAGE_FILE_DOES_NOT_EXIST, $this->application->getDisplay());
        $this->assertContains(PhpccCommand::MESSAGE_NO_FILES_TO_CHECK, $this->application->getDisplay());
    }

    /**
     * @test
     */
    public function nonexistentPhpFileShouldDisplayNonexistentFileMessage()
    {
        $this->application->run(['nonexistentFile.php']);

        $this->assertContains(PhpccCommand::MESSAGE_FILE_DOES_NOT_EXIST, $this->application->getDisplay());
    }

    /**
     * @test
     */
    public function badSyntaxFileShouldReturnErrorCode()
    {
        $codeToCheck = <<<'EOT'
<?php

return

EOT;

        $this->checkFile = '/tmp/phpccSyntaxCheck.php';
        file_put_contents($this->checkFile, $codeToCheck);

        $this->application->run([$this->checkFile]);

        $this->assertSame(SyntaxException::CODE, $this->application->getStatusCode());
    }

    /**
     * @test
     */
    public function badCodeStyleFileShouldBeFixed()
    {
        $codeToCheck = <<<'EOT'
<?php
namespace A;
use B;

EOT;

        $this->checkFile = '/tmp/phpccCodeStyleCheck.php';
        file_put_contents($this->checkFile, $codeToCheck);

        $this->application->run([$this->checkFile, '--fix-cs' => null]);

        $this->assertSame(PhpccCommand::STATUS_CODE_SUCCESS, $this->application->getStatusCode());
    }

    /**
     * @test
     */
    public function badCodeStyleFileShouldReturnErrorCode()
    {
        $codeToCheck = <<<'EOT'
<?php
namespace A;
use B;

EOT;

        $this->checkFile = '/tmp/phpccCodeStyleCheck.php';
        file_put_contents($this->checkFile, $codeToCheck);

        $this->application->run([$this->checkFile]);

        $this->assertSame(CodeStyleException::CODE, $this->application->getStatusCode());
    }

    /**
     * @test
     */
    public function codeMessFileShouldReturnErrorCode()
    {
        $codeToCheck = <<<'EOT'
<?php

function doNothing($a)
{
}

EOT;

        $this->checkFile = '/tmp/phpccCodeMessCheck.php';
        file_put_contents($this->checkFile, $codeToCheck);

        $this->application->run([$this->checkFile]);

        $this->assertSame(CodeMessException::CODE, $this->application->getStatusCode());
    }

    /**
     * @test
     */
    public function correctFileShouldReturnCorrectStatusCode()
    {
        $absolutePath = __FILE__;

        $this->application->run([$absolutePath]);

        $this->assertNotContains(PhpccCommand::MESSAGE_FILE_DOES_NOT_EXIST, $this->application->getDisplay());
        $this->assertSame(PhpccCommand::STATUS_CODE_SUCCESS, $this->application->getStatusCode());

        $relativePath = substr(__FILE__, strlen(getcwd()) + 1);

        $this->application->run([$relativePath]);

        $this->assertNotContains(PhpccCommand::MESSAGE_FILE_DOES_NOT_EXIST, $this->application->getDisplay());
        $this->assertSame(PhpccCommand::STATUS_CODE_SUCCESS, $this->application->getStatusCode());
    }
}
