# PHP Code Checker

CLI tool to run PHP code checks that can be used as git pre-commit hook.

See **Usage** section below.


## Installation

Install composer:
```
curl -LsS https://getcomposer.org/installer -o composer-setup.php
php composer-setup.php && rm composer-setup.php
mkdir -p ~/.local/bin
mv composer.phar ~/.local/bin/composer
sudo ln -sf ~/.local/bin/composer /usr/local/bin/composer
```

Install PHP Code Checker:
```
composer global require alfonsomthd/phpcc
sudo ln -sf $(composer global config --absolute vendor-dir 2>/dev/null)/alfonsomthd/phpcc/phpcc /usr/local/bin/phpcc
```


## Configuration

The following tasks are executed:

* Syntax check (lint).
* Code style fixing with php-cs-fixer.
* Tests execution -if there are any- with PHPUnit when checking files in staging area.
* Code analysis with phpmd.

If you want to override the default configuration, create a file in your project root
named *phpcc.json* with the following structure (example with default configuration values):

    {
        "ignore": [],
        "checks": {
            "php-cs-fixer": {
                "rules": "@PSR2,@PSR1,-psr0",
                "fix": true
            },
            "phpmd": {
                "ruleset": "",
                "minimumpriority": 2
            },
            "phpunit": {
                "config-file": "",
                "allow-display": true
            }
        }
    }

Configuration fields:

* **ignore**
    
    Like git, you can ignore files/folders adding the relative path in this field. Also, you can use the prefix `!` to negate the path and not ignore the file/folder.
    
    For example, the following configuration ignore all the project except the directories `src` and `tests`:
        
        "ignore": [".", "!src", "!tests"]

* **checks**
    * **php-cs-fixer**
        * rules: If not set, default value (see example above) will be used. See php-cs-fixer documentation for allowed values.
        * fix: If not set, default value is *true* only when cheking files in git staging area.
            Option **--fix-cs** overrides everything else.

    * **phpmd**
        * ruleset: Relative path to rule set filename or a comma-separated string of rule set filenames.
            If not set, phpcc rule set *phpmdRuleSet.xml* will be used.
            See phpmd documentation for rule set filenames already provided by phpmd.
        * minimumpriority: If not set, default value (see example above) will be used. See phpmd documentation.
    
    * **phpunit**
        * config-file: Relative path to *phpunit.xml*. If you want to generate a new configuration file, run:
            ```
            phpunit --generate-configuration
            ```
        * allow-display: *false* to disable progress display. Allowed by default.
            In case of errors, this setting will be ignored and all output will be displayed.


## Usage

The tool must be run in your project's root directory
(the directory that contains your project's composer.json):
```
cd <YOUR_PROJECT_ROOT_DIRECTORY>
```

Display the help message:
```
phpcc -h
```

To check a file or directory, pass it as first argument (e.g.):
```
phpcc src
```

Check a file or directory fixing code style errors:
```
phpcc src --fix-cs
```

To check PHP files in git staging area:
```
phpcc
```

To run it on git pre-commit, do the following:
```
ln -sf /usr/local/bin/phpcc .git/hooks/pre-commit
```
